<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
	<title>Pruebas Ajax History</title>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.history.js"></script>
</head>
<body>
<audio controls>
  <source src="audio/audio.mp3" type="audio/mpeg">
  Your browser does not support this audio format.
</audio> 
<ul id="nav">
	<li><a href="inicio.php">Inicio</a></li>
	<li><a href="estudio.php">Estudio</a></li>
	<li><a href="ensayos.php">Ensayos</a></li>
	<li><a href="contactos.php">Contactos</a></li>
</ul>
<div id="master_content">
	
</div>
<script type='text/javascript'>

$(window).load(function(){
	$(function() {
	    $.history.on('load change push', function(event, url, type) {
	        $("#master_content").load(url);
	    }).listen('hash');
	    
	    $('body').on('click', 'a', function(event){
	        $.history.push($(this).attr('href'));
	        event.preventDefault();
	    });
	    
	    $('button').click(function(){
	        location.reload();
	    }); 
	});
});

</script>
</body>
</html>
